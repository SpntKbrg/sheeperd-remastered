package com.quaruzuz.sheeperd.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.quaruzuz.sheeperd.SheeperdGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Sheeperd Remastered";
        config.width = 1280;
        config.height = 720;
        new LwjglApplication(new SheeperdGame(), config);
    }
}
