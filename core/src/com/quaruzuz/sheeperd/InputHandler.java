package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

public class InputHandler {
    public static float getY() {
        return 720.0f * ((float) (Gdx.graphics.getHeight() - Gdx.input.getY()) / (float) Gdx.graphics.getHeight());
    }
    
    public static float getX() {
        return 1280.0f * (Gdx.input.getX() / (float) Gdx.graphics.getWidth());
    }
    
    public static void render(GameScreen gameScreen) {
        gameScreen.batch.draw(AssetManager.cursorIcon, InputHandler.getX() - 16, InputHandler.getY() - 16);
    }
    
    public void processInput(GameScreen gameScreen) {
        if (Gdx.input.justTouched()) {
//            System.out.println("Click Detected");
            // Process Mouse Click
            if (getX() > 720.0f - InitConst.TOOLBOX_SIZE) {
                gameScreen.toolBox.processClick();
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            //            gameScreen.gameWorld.sheepManager.spawnSheep(gameScreen.gameWorld, getX(), getY());
            gameScreen.gameWorld.sheepManager.spawnSheep(gameScreen.gameWorld);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            //            gameScreen.gameWorld.wolfManager.spawnSheep(gameScreen.gameWorld, getX(), getY());
            gameScreen.gameWorld.wolfManager.spawn(gameScreen.gameWorld);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            gameScreen.economy.addMoney(1000);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            gameScreen.economy.spendMoney(1000);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            gameScreen.gameWorld.gameClock.setHour(6);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
            gameScreen.gameWorld.gameClock.setHour(19);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            gameScreen.gameWorld.timeFlow = !(gameScreen.gameWorld.timeFlow);
        }
    }
}
