package com.quaruzuz.sheeperd;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

public class Grass {
    
    public Rectangle grassPatch;
    public float grassValue;
    public long growTime;
    public int state = 0;
    
    public Grass() {
        this.grassPatch = new Rectangle(MathUtils.random(InitConst.FENCE_THICK, 1280 - InitConst.FENCE_THICK - InitConst.TOOLBOX_SIZE), MathUtils.random(0 + InitConst.FENCE_THICK, 720 - InitConst.FENCE_THICK), InitConst.GRASS_SIZE,
                                        InitConst.GRASS_SIZE);
        this.grassValue = 10;
        this.growTime = TimeUtils.millis();
    }
    
    public Grass(int value) {
        this.grassPatch = new Rectangle(MathUtils.random(100, 800), MathUtils.random(100, 364), InitConst.GRASS_SIZE,
                                        InitConst.GRASS_SIZE);
        this.grassValue = value;
        this.growTime = TimeUtils.millis();
    }
    
    public Grass(int x, int y) {
        this.grassPatch = new Rectangle(x, y, InitConst.GRASS_SIZE, InitConst.GRASS_SIZE);
        this.grassValue = 10;
    }
    
    public void draw(GameScreen gameScreen) {
        gameScreen.batch.draw(AssetManager.grassPatch, grassPatch.x, grassPatch.y, InitConst.GRASS_SIZE / 2,
                              InitConst.GRASS_SIZE / 2, InitConst.GRASS_SIZE, InitConst.GRASS_SIZE, grassValue / 100.0f,
                              grassValue / 100.0f, 0, 0, 0, InitConst.GRASS_SIZE, InitConst.GRASS_SIZE, false, false);
    }
}
