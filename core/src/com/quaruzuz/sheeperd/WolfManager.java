package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class WolfManager {
    
    public Array<CharacterPhysics> wolfGroup;
    
    public WolfManager() {
        wolfGroup = new Array<CharacterPhysics>();
        
    }
    
    public void render(GameScreen gameScreen) {
        if (gameScreen.gameWorld.gameClock.isNightTime() && wolfGroup.size < 1) {
            spawn(gameScreen.gameWorld, MathUtils.random(0, InitConst.WOLF_SPAWNS));
        }
        if (wolfGroup.size > 0) {
            for (CharacterPhysics wolf : wolfGroup) {
                WolfEyes.draw(wolf);
                WolfBehavior.simulate(wolf, gameScreen);
                WolfRenderer.draw(wolf, gameScreen);
                Bubble.draw(wolf, gameScreen, 2);
            }
        }
    }
    
    // Wolf Spawning
    public void spawn(GameWorld gameWorld, int number) {
        for (int index = 0; index < number; index = index + 1) {
            spawn(gameWorld);
        }
    }
    
    public void spawn(GameWorld gameWorld) {
        spawn(gameWorld, (float) MathUtils.random(200, 1000), (float) MathUtils
                .random(InitConst.FENCE_THICK + InitConst.WOLF_SIZE, InitConst.FENCE_THICK + InitConst.WOLF_SIZE + 30));
    }
    
    public void spawn(GameWorld gameWorld, float posX, float posY) {
        wolfGroup.add(new CharacterPhysics(gameWorld, posX, posY, InitConst.WOLF_SIZE, InitConst.WOLF_INIT_HUNGER));
    }
    
    public static void killWolf(GameWorld gameWorld, CharacterPhysics wolf) {
        CharacterPhysics tempWolf = wolf;
        gameWorld.wolfManager.wolfGroup.removeValue(wolf, false);
        tempWolf.getBody().setTransform(-200.0f, -200.0f, 0.0f);
        tempWolf.getBody().setActive(false);
        tempWolf.getEyeLeft().setActive(false);
        tempWolf.getEyeRight().setActive(false);
        tempWolf.getEyeLeft().remove();
        tempWolf.getEyeRight().remove();
        gameWorld.sheepWorld.destroyBody(tempWolf.getBody());
    }
}
