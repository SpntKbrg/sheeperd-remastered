package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameScreen extends ScreenAdapter {
    
    private SheeperdGame sheeperdGame;
    
    OrthographicCamera camera;
    SpriteBatch batch;
    
    GameWorld gameWorld;
    Economy economy;
    Toolbox toolBox;
    SheepText sheepFont;
    
    InputHandler inputHandler;
    
    public GameScreen(SheeperdGame sheeperdGame) {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 1280, 720);
        
        this.sheeperdGame = sheeperdGame;
        
        this.toolBox = new Toolbox(this);
        this.gameWorld = new GameWorld(this);
        this.economy = new Economy();
        
        this.inputHandler = new InputHandler();
        
        this.batch = new SpriteBatch();
        this.sheepFont = new SheepText(this.batch);
    }
    
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.4f, 0.8f, 0.2f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        camera.update();
        inputHandler.processInput(this);
        
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(AssetManager.bg, 0, 0, 1155, 720, 0, 0, 1155, 720, false, false);
        toolBox.draw();
        sheepFont.write(String.valueOf(economy.getMoney()), 1280 - InitConst.TOOLBOX_SIZE + InitConst.FENCE_THICK, 24);
        gameWorld.render();
        
        batch.draw(AssetManager.grassNotifLite, -200, -200, 0, 0, 1, 1);
        
        batch.end();
        
        gameWorld.lighting.render(gameWorld.gameClock.gameTime);
    }
    
    public void gameOver() {
        sheeperdGame.gameOver();
    }
}