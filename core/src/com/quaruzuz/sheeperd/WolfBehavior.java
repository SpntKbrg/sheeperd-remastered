package com.quaruzuz.sheeperd;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.TimeUtils;

public class WolfBehavior {
    
    public static void simulate(CharacterPhysics wolf, GameScreen gameScreen) {
        // Daytime De-spawning
        if (gameScreen.gameWorld.gameClock.isDaytime()) {
            wolf.getBody().setType(BodyDef.BodyType.KinematicBody);
            runAway(wolf);
            if (GameUtils.outOfScreen(wolf)) {
                WolfManager.killWolf(gameScreen.gameWorld, wolf);
            }
            return;
        }
        
        float hungerDist = (InitConst.WOLF_HUNGER_DECAY_TIME / 100.0f);
        if (wolf.hunger <= 0) {
            WolfManager.killWolf(gameScreen.gameWorld, wolf);
        }
        if (GameUtils.timerCheck(wolf.eatSeconds, hungerDist)) {
            if (sheepCheck(true, wolf, gameScreen)) {
                wolf.hunger = wolf.hunger + 50;
                return;
            } else if (!moveFromCursor(wolf)) {
                if (wolf.hunger > 30) {
                    chaseSheep(wolf, gameScreen, 0);
                } else {
                    chaseSheep(wolf, gameScreen, InitConst.WOLF_HUNGRY_MOD);
                }
            }
            if (GameUtils.timerCheck(wolf.hungerSeconds, hungerDist)) {
                wolf.hunger = wolf.hunger - 0.5f;
                wolf.hungerSeconds = TimeUtils.millis();
            }
        } else {
            wolf.bubble.state = 4;
            idleWolf(wolf);
        }
    }
    
    private static void runAway(CharacterPhysics wolf) {
        GameUtils.turnsTowards(wolf, wolf.getPosition().x, -100);
        wolf.getBody().setLinearVelocity(0, -4000);
    }
    
    private static void idleWolf(CharacterPhysics wolf) {
        if (GameUtils.timerCheck(wolf.idleSeconds, InitConst.WOLF_IDLE_TIME)) {
            wolf.getBody()
                .applyAngularImpulse((MathUtils.random(-InitConst.WOLF_IDLE_ROLL, InitConst.WOLF_IDLE_ROLL)), true);
            wolf.getBody().applyForceToCenter((float) (MathUtils.random(0.0f, (float) InitConst.WOLF_IDLE_SPEED) *
                                                       Math.cos(wolf.getBody().getAngle())),
                                              (float) (MathUtils.random(0.0f, (float) InitConst.WOLF_IDLE_SPEED) *
                                                       Math.sin(wolf.getBody().getAngle())), true);
            wolf.idleSeconds = TimeUtils.millis() - (long) (0.5 * InitConst.WOLF_IDLE_TIME);
        }
    }
    
    private static void chaseSheep(CharacterPhysics wolf, GameScreen gameScreen, int modifier) {
        CharacterPhysics targetSheep;
        boolean sheepFound = false;
        float targetDistance = (float) Math.pow(InitConst.WOLF_SHEEP_DETECT, 2);
        if (gameScreen.gameWorld.sheepManager.sheepGroup.size > 0) {
            targetSheep = gameScreen.gameWorld.sheepManager.sheepGroup.first();
        } else {
            return;
        }
        for (CharacterPhysics sheep : gameScreen.gameWorld.sheepManager.sheepGroup) {
            if (Math.pow((wolf.getPosition().x - sheep.getPosition().x - InitConst.SHEEP_SIZE / 2), 2) +
                Math.pow((wolf.getPosition().y - sheep.getPosition().y - InitConst.SHEEP_SIZE / 2), 2) <
                targetDistance) {
                targetSheep = sheep;
                targetDistance = GameUtils.getDistanceSQ(wolf.getPosition().x, wolf.getPosition().y,
                                                         sheep.getPosition().x - InitConst.SHEEP_SIZE / 2,
                                                         sheep.getPosition().y - InitConst.SHEEP_SIZE / 2);
                sheepFound = true;
            }
        }
        if (sheepFound) {
            GameUtils.turnsAway(wolf, targetSheep.getPosition().x + InitConst.SHEEP_SIZE / 2,
                                   targetSheep.getPosition().y + InitConst.SHEEP_SIZE / 2);
            GameUtils.normalizeApply(wolf, targetSheep.getPosition().x, targetSheep.getPosition().y,
                                     wolf.getPosition().x, wolf.getPosition().y, InitConst.WOLF_SHEEP_SPEED);
        }
    }
    
    private static boolean moveFromCursor(CharacterPhysics wolf) {
        if (Math.pow((InputHandler.getX() - wolf.getPosition().x), 2) +
            Math.pow((InputHandler.getY() - wolf.getPosition().y), 2) < Math.pow(InitConst.WOLF_CURSOR_DETECT, 2)) {
            GameUtils.turnsAway(wolf, InputHandler.getX(), InputHandler.getY());
            GameUtils.normalizeApply(wolf, InputHandler.getX(), InputHandler.getY(), wolf.getPosition().x,
                                     wolf.getPosition().y, -InitConst.WOLF_CURSOR_SPEED);
            return true;
        }
        return false;
    }
    
    public static boolean sheepCheck(boolean eat, CharacterPhysics wolf, GameScreen gameScreen) {
        for (CharacterPhysics sheep : gameScreen.gameWorld.sheepManager.sheepGroup) {
            if (GameUtils.checkCircleOverlap(wolf.getPosition().x, wolf.getPosition().y, InitConst.WOLF_SIZE * 1.2f,
                                             sheep.getPosition().x, sheep.getPosition().y, InitConst.SHEEP_SIZE)) {
                if (eat) {
                    wolf.eatSeconds = TimeUtils.millis();
                    gameScreen.gameWorld.sheepManager.killSheep(sheep);
                    SheepBehavior.awoke();
                }
                return true;
            }
        }
        return false;
    }
}
