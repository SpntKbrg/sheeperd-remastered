package com.quaruzuz.sheeperd;

import box2dLight.ConeLight;
import box2dLight.Light;
import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.TimeUtils;
import org.omg.CORBA.TIMEOUT;

class CharacterPhysics {
    
    private BodyDef bodyDef;
    private Body body;
    private FixtureDef fixtureDef;
    private Fixture fixture;
    private Light eyeLeft;
    private Light eyeRight;
    
    Bubble bubble;
    
    boolean starvingMode = false;
    int status = 0;
    
    float hunger;
    long bornSeconds;
    long idleSeconds;
    long hungerSeconds;
    long eatSeconds;
    
    CharacterPhysics(GameWorld world, float posX, float posY, int size, int starting_hunger) {
        bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.linearDamping = 1.0f;
        bodyDef.angularDamping = 0.999f;
        bodyDef.position.set(posX, posY);
        
        body = world.sheepWorld.createBody(bodyDef);
        
        CircleShape circle = new CircleShape();
        circle.setRadius(size);
        
        fixtureDef = new FixtureDef();
        fixtureDef.shape = circle;
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 1.0f;
        fixtureDef.restitution = 0.0f;
        
        fixture = body.createFixture(fixtureDef);
        
        circle.dispose();
        
        eyeLeft = new ConeLight(world.lighting.rayHandler, InitConst.EYERAYS, null, InitConst.EYE_GLOW, body.getPosition().x - (size + 2), body.getPosition().y + (size / 5.0f),
                                (float) (-Math.toDegrees(body.getAngle())), InitConst.EYE_CONE);
        eyeLeft.setActive(false);
        eyeRight = new ConeLight(world.lighting.rayHandler, InitConst.EYERAYS, null, InitConst.EYE_GLOW, body.getPosition().x - (size + 2), body.getPosition().y  - (size / 5.0f),
                                 (float) (-Math.toDegrees(body.getAngle())), InitConst.EYE_CONE);
        eyeRight.setActive(false);
        eyeLock(body);
        
        body.setTransform(body.getPosition().x, body.getPosition().y, MathUtils.random((float) - Math.PI, (float) Math.PI));
        
        bubble = new Bubble();
        idleSeconds = TimeUtils.millis();
        hungerSeconds = TimeUtils.millis();
        eatSeconds = TimeUtils.millis();
        bornSeconds = TimeUtils.millis();
        hunger = starting_hunger;
        
    }
    
    private void eyeLock(Body body) {
        eyeLeft.attachToBody(body);
        eyeRight.attachToBody(body);
    }
    
    public Body getBody() {
        return body;
    }
    
    public Vector2 getPosition() {
        return body.getPosition();
    }
    
    public Light getEyeLeft() {
        return eyeLeft;
    }
    
    public Light getEyeRight() {
        return eyeRight;
    }
    
    public Fixture getFixture() {
        return fixture;
    }
}
