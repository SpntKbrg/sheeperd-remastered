package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SheeperdGame extends Game {
    SpriteBatch batch;
    
    @Override
    public void create() {
        AssetManager.load();
        batch = new SpriteBatch();
        setScreen(new MenuScreen(this));
    }
    
    @Override
    public void render() {
        super.render();
    }
    
    @Override
    public void dispose() {
        batch.dispose();
    }
    
    public void gameOver() {
        setScreen(new SheepOverScreen(this));
    }
    
    public void gameStart() {
        setScreen(new GameScreen(this));
    }
}
