package com.quaruzuz.sheeperd;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class GrassManager {
    
    public Array<Grass> grassGroup;
    private long grassTime;
    
    public GrassManager() {
        this.grassGroup = new Array<Grass>();
        this.grassTime = TimeUtils.millis();
        growGrass(100);
    }
    
    public void growGrass() {
        grassGroup.add(new Grass());
    }
    
    public void growGrass(int value) {
        grassGroup.add(new Grass(value));
    }
    
    public void growGrass(int posX, int posY) {
        grassGroup.add(new Grass(posX, posY));
    }
    
    public void render(GameScreen gameScreen) {
        if (GameUtils.timerCheck(grassTime, InitConst.GRASS_SPAWN_TIME)) {
            grassTime = TimeUtils.millis();
            growGrass();
        }
        if (grassGroup.size < 1) {
            growGrass();
        }
        for (Grass grass : this.grassGroup) {
            //            System.out.println(grass.grassValue);
            if (grass.grassValue < 0) {
                grassGroup.removeValue(grass, true);
                continue;
            }
            if (GameUtils.timerCheck(grass.growTime, InitConst.GRASS_GROW_TIME)) {
                if (grass.state == 1) {
                    grass.grassValue = grass.grassValue * 1.02f;
                } else if (grass.state == 0) {
                    grass.grassValue = grass.grassValue * 1.2f;
                }
                grass.growTime = TimeUtils.millis();
            }
            if (grass.grassValue > 100) {
                grass.grassValue = 100;
                grass.state = 1;
            }
            //            System.out.println(this.grassGroup.size);
            grass.draw(gameScreen);
        }
    }
}
