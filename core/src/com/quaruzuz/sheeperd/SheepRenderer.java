package com.quaruzuz.sheeperd;

public class SheepRenderer {
    
    public static void draw(CharacterPhysics sheep, GameScreen gameScreen) {
        if (sheep.status == 2) {
            gameScreen.batch.draw(AssetManager.sheepTexture, sheep.getPosition().x - (30),
                                  (float) (sheep.getPosition().y - 20), (float) 28.5, 21, 57, 42,
                                  (float) (0.05 * InitConst.SHEEP_SIZE), (float) (0.05 * InitConst.SHEEP_SIZE),
                                  (float) Math.toDegrees(sheep.getBody().getAngle()), 0, 0, 57, 42, false, false);
        } else if (sheep.status == 1) {
            gameScreen.batch.draw(AssetManager.lambTexture, sheep.getPosition().x - (30),
                                  (float) (sheep.getPosition().y - 20), (float) 28.5, 21, 57, 42,
                                  (float) (0.1 * InitConst.LAMB_SIZE), (float) (0.1 * InitConst.LAMB_SIZE),
                                  (float) Math.toDegrees(sheep.getBody().getAngle()), 0, 0, 57, 42, false, false);
        }
    }
}
