package com.quaruzuz.sheeperd;

public class WolfEyes {
    
    public static void draw(CharacterPhysics wolf) {
        wolf.getEyeLeft().setDirection(1800 - (float) Math.toDegrees(wolf.getBody().getAngle()));
        wolf.getEyeLeft().setDistance(InitConst.WOLF_EYE_GLOW);
        wolf.getEyeLeft().setColor(0.99f, 0.0f, 0.1f, 0.2f);
        wolf.getEyeLeft().setActive(true);
        wolf.getEyeRight().setDirection(180 -  (float) Math.toDegrees(wolf.getBody().getAngle()));
        wolf.getEyeRight().setDistance(InitConst.WOLF_EYE_GLOW);
        wolf.getEyeRight().setColor(0.99f, 0.0f, 0.1f, 0.2f);
        wolf.getEyeRight().setActive(true);
    }
}
