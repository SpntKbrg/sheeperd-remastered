package com.quaruzuz.sheeperd;

import com.badlogic.gdx.math.MathUtils;

public class Bubble {
    
    int state = 0;
    
    public Bubble() {
        this.state = 3;
    }
    
    public static void draw(CharacterPhysics character, GameScreen gameScreen, int type) {
        if (type == 1) { // Sheep
            if (character.bubble.state == 0) {
                return;
            } else if (character.bubble.state == 1) {
                gameScreen.batch.draw(AssetManager.grassNotifLite, character.getPosition().x,
                                      character.getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            } else if (character.bubble.state == 2) {
                gameScreen.batch.draw(AssetManager.grassNotifFull, character.getPosition().x,
                                      character.getBody().getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            } else if (character.bubble.state == 3) {
                gameScreen.batch.draw(AssetManager.grassNotifFull, character.getPosition().x + MathUtils
                                              .random(-5.0f / (character.hunger + 1), 5.0f / (character.hunger + 1)),
                                      character.getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            } else if (character.bubble.state == 4) {
                gameScreen.batch.draw(AssetManager.grassNotifEating, character.getPosition().x,
                                      character.getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            } else if (character.bubble.state == 5) {
                gameScreen.batch.draw(AssetManager.grassNotifSleep, character.getPosition().x,
                                      character.getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            } else if (character.bubble.state == 6) {
                gameScreen.batch.draw(AssetManager.grassNotifAwoke, character.getPosition().x,
                                      character.getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            }
        } else if (type == 2) { // Wolf
            if (character.bubble.state == 0) {
                return;
            } else if (character.bubble.state == 1) {
                gameScreen.batch.draw(AssetManager.grassNotifLite, character.getPosition().x,
                                      character.getBody().getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            } else if (character.bubble.state == 2) {
                gameScreen.batch.draw(AssetManager.grassNotifFull, character.getPosition().x,
                                      character.getPosition().y + 10, 0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f,
                                      0, 0, 64, 64, false, false);
            } else if (character.bubble.state == 3) {
                gameScreen.batch.draw(AssetManager.grassNotifFull, character.getPosition().x + MathUtils
                                              .random(-5.0f / character.hunger, .0f / character.hunger), character.getPosition().y + 10,
                                      0.0f, 0.0f, 64.0f, 64.0f, 0.8f, 0.8f, 0.0f, 0, 0, 64, 64, false, false);
            }
        }
    }
}
