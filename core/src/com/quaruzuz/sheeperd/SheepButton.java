package com.quaruzuz.sheeperd;

import com.badlogic.gdx.math.Rectangle;

public class SheepButton extends Rectangle {
    public int content = 0;
    public long cost = 0;
    
    public SheepButton(int content, int index) {
        this.content = content;
        switch (content) {
            case 1: {
                cost = InitConst.LAMB_COST;
                break;
            }
            case 2: {
                cost = InitConst.GRASS_COST;
                break;
            }
        }
        this.x = 1280 - InitConst.FENCE_THICK - InitConst.BUTTON_WIDTH;
        this.y = InitConst.FENCE_THICK * 2 + (index * InitConst.BUTTON_HEIGHT);
    }
    
    public void draw(GameScreen gameScreen, int index) {
        gameScreen.batch.draw(AssetManager.buttonBG, this.x, this.y);
        switch (content) {
            case 1: {
                gameScreen.batch.draw(AssetManager.sheepTexture, this.x + 10,
                                      this.y + 20, (float) 28.5, 21, 57, 42,
                                      (float) (0.05 * InitConst.SHEEP_SIZE), (float) (0.05 * InitConst.SHEEP_SIZE), 0,
                                      0, 0, 57, 42, false, false);
                break;
            }
            case 2: {
                gameScreen.batch.draw(AssetManager.grassPatch, this.x - 90,
                                      this.y - 90, InitConst.GRASS_SIZE / 2,
                                      InitConst.GRASS_SIZE / 2, InitConst.GRASS_SIZE, InitConst.GRASS_SIZE, 0.1f,
                                      0.1f, 0, 0, 0, InitConst.GRASS_SIZE, InitConst.GRASS_SIZE, false, false);
                break;
            }
        }
        gameScreen.sheepFont.write(String.valueOf(cost), this.x + 15, this.y + 20);
    }
    
    public void clicked(GameScreen gameScreen) {
//        System.out.println("Button Pressed");
        if (gameScreen.economy.getMoney() < cost) {
            return;
        }
        switch (content) {
            case 1: {
                gameScreen.gameWorld.sheepManager.spawnLamb();
                break;
            }
            case 2: {
                gameScreen.gameWorld.grassManager.growGrass(30);
                break;
            }
        }
        gameScreen.economy.spendMoney(cost);
    }
}
