package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MenuScreen extends ScreenAdapter{
    public SheeperdGame sheeperdGame;
    public SpriteBatch batch;
    public OrthographicCamera camera;
    public SheepText sheepFont;
    
    public MenuScreen(SheeperdGame sheeperdGame) {
        this.sheeperdGame = sheeperdGame;
        
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 1280, 720);
        
        this.batch = new SpriteBatch();
        this.sheepFont = new SheepText(batch);
        
        AssetManager.gameMenuMusic.setLooping(true);
        AssetManager.gameMenuMusic.setVolume(InitConst.MUSIC_VOL);
        AssetManager.gameMenuMusic.play();
    }
    
    @Override
    public void render(float delta) {
        if(Gdx.input.justTouched()) {
            AssetManager.gameMenuMusic.stop();
            sheeperdGame.gameStart();
            return;
        }
        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
//        batch.draw(AssetManager.bg, 0, 0, 1155, 720, 0, 0, 1155, 720, false, false);
        batch.draw(AssetManager.logo, 0, 300);
        batch.draw(AssetManager.grassNotifLite, -200, -200, 0, 0, 1, 1);
        sheepFont.write("Click to Start", 500, 200);
    
        batch.end();
    }
}
