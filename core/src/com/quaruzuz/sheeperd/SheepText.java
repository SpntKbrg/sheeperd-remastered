package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SheepText {
    private BitmapFont font;
    private SpriteBatch batch;
    
    public SheepText(SpriteBatch batch) {
        this.batch = batch;
        font = new BitmapFont(Gdx.files.internal("fonts/Silkscreen.fnt"));
    }
    
    public void write(String input, float x, float y) {
        font.draw(batch, input, x ,y);
    }
}
