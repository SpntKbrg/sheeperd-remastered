package com.quaruzuz.sheeperd;

import com.badlogic.gdx.utils.TimeUtils;

public class GameUtils {
    public static float angleToPoint(float aX, float aY, float bX, float bY) {
        float angle;
        if (bX - aX > 0) {
            angle = (float) Math.atan((bY - aY) / (bX - aX));
        } else {
            if (bY - aY > 0) {
                angle = (float) (Math.PI) + (float) Math.atan((bY - aY) / (bX - aX));
            } else {
                angle = (float) (-Math.PI) + (float) Math.atan((bY - aY) / (bX - aX));
            }
            
        }
        return angle;
    }
    
    public static float inverseAngleToPoint(float aX, float aY, float bX, float bY) {
        return (float) Math.PI + GameUtils.angleToPoint(aX, aY, bX, bY);
    }
    
    public static boolean checkCircleOverlap(float aX, float aY, float aSize, float bX, float bY, float bSize) {
        return (aX + (aSize * InitConst.OVERLAP_ALLOW) > bX && aY + (aSize * InitConst.OVERLAP_ALLOW) > bY &&
                aX - (aSize * InitConst.OVERLAP_ALLOW) < bX && aY - (aSize * InitConst.OVERLAP_ALLOW) < bY);
    }
    
    public static boolean timerCheck(long milliTime, float timeNeeded) {
        return (TimeUtils.timeSinceMillis(milliTime) > timeNeeded * 1000);
    }
    
    public static float getDistanceSQ(float aX, float aY, float bX, float bY) {
        return (float) (Math.pow((aX - bX), 2) + Math.pow((aY - bY), 2));
    }
    
    public static float getDistance(float aX, float aY, float bX, float bY) {
        return (float) (Math.sqrt(GameUtils.getDistanceSQ(aX, aY, bX, bY)));
    }
    
    public static void normalizeApply(CharacterPhysics character, float aX, float aY, float bX, float bY,
                                      float multiplier) {
        float forceX = multiplier * (aX - bX) / (GameUtils.getDistance(aX, aY, bX, bY));
        float forceY = multiplier * (aY - bY) / (GameUtils.getDistance(aX, aY, bX, bY));
        character.getBody().applyForceToCenter(forceX, forceY, true);
    }
    
    public static void turnsTowards(CharacterPhysics character, float x, float y) {
        character.getBody().setTransform(character.getPosition().x, character.getPosition().y, GameUtils
                .angleToPoint(x, y, character.getPosition().x, character.getPosition().y));
    }
    
    public static void turnsAway(CharacterPhysics character, float x, float y) {
        character.getBody().setTransform(character.getPosition().x, character.getPosition().y, GameUtils
                .inverseAngleToPoint(x, y, character.getPosition().x, character.getPosition().y));
    }
    
    public static boolean outOfScreen(CharacterPhysics character) {
        return (character.getPosition().x > 1310 || character.getPosition().x < -30) ||
               (character.getPosition().y > 750 || character.getPosition().y < -30);
    }
}
