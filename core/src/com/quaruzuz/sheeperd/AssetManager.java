package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

class AssetManager {
    
    public static Texture bg;
    public static Texture logo;
    public static Texture toolBar;
    public static Texture grassNotifEating;
    public static Texture grassNotifLite;
    public static Texture grassNotifFull;
    public static Texture grassNotifSleep;
    public static Texture grassNotifAwoke;
    public static Texture sheepTexture;
    public static Texture lambTexture;
    public static Texture wolfTexture;
    public static Texture grassPatch;
    public static Texture cursorIcon;
    public static Texture buttonBG;
    
    public static Music gameMenuMusic;
    public static Music gameDayMusic;
    public static Music gameNightMusic;
    
    
    public static void load() {
        bg = new Texture(Gdx.files.internal("images/DirtBG.png"));
        logo = new Texture(Gdx.files.internal("images/Logo.png"));
        buttonBG = new Texture(Gdx.files.internal("sprites/Button.png"));
        toolBar = new Texture(Gdx.files.internal("images/Toolbar.png"));
        
        grassNotifEating = new Texture(Gdx.files.internal("sprites/HungerNotifEating.png"));
        grassNotifLite = new Texture(Gdx.files.internal("sprites/HungerNotifLite.png"));
        grassNotifFull = new Texture(Gdx.files.internal("sprites/HungerNotifFull.png"));
        grassNotifSleep = new Texture(Gdx.files.internal("sprites/HungerNotifSleep.png"));
        grassNotifAwoke = new Texture(Gdx.files.internal("sprites/HungerNotifWoken.png"));
        
        sheepTexture = new Texture(Gdx.files.internal("sprites/Sheep.png"));
        lambTexture = new Texture(Gdx.files.internal("sprites/Lamb.png"));
        wolfTexture = new Texture(Gdx.files.internal("sprites/Wolf.png"));
        
        grassPatch = new Texture(Gdx.files.internal("images/GrassPatch.png"));
        
        cursorIcon = new Texture(Gdx.files.internal("sprites/Cursor.png"));
        
        gameMenuMusic = Gdx.audio.newMusic(Gdx.files.internal("music/BGM_Menu.mp3"));
        gameDayMusic = Gdx.audio.newMusic(Gdx.files.internal("music/BGM_Day.mp3"));
        gameNightMusic = Gdx.audio.newMusic(Gdx.files.internal("music/BGM_Night.mp3"));
    }
}
