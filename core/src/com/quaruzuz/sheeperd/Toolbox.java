package com.quaruzuz.sheeperd;

import com.badlogic.gdx.utils.Array;

public class Toolbox {
    
    GameScreen gameScreen;
    private Array<SheepButton> buttons;
    
    public Toolbox(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
        buttons = new Array<SheepButton>();
        buttons.add(new SheepButton(1, buttons.size));
        buttons.add(new SheepButton(2, buttons.size));
    }
    
    public void draw() {
        gameScreen.batch.draw(AssetManager.toolBar, 1155, 0, 125, 720, 0, 0, 125, 720, false, false);
        int buttonNumber = 0;
        for (SheepButton button : buttons) {
            button.draw(gameScreen, buttonNumber);
            buttonNumber = buttonNumber + 1;
        }
    }
    
    public void processClick() {
        for (SheepButton button : buttons) {
            if (InputHandler.getY() > button.y && InputHandler.getY() < button.y + InitConst.BUTTON_HEIGHT) {
                button.clicked(gameScreen);
            }
        }
    }
}
