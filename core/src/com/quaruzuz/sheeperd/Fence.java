package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

class Fence {
    
    private BodyDef fenceBodyDef;
    private Body fenceBody;
    
    private World sheepWorld;
    
    public Fence(World sheepWorld, int type, float location) {
        this.sheepWorld = sheepWorld;
        // Determine Layout
        fenceBodyDef = new BodyDef();
        fenceBodyDef.type = BodyDef.BodyType.KinematicBody;
        if (type == 0) { // Horizontal
            fenceBodyDef.position.set(new Vector2(0, location + InitConst.FENCE_THICK));
        } else { // Vertical
            fenceBodyDef.position.set(new Vector2(location + InitConst.FENCE_THICK, 0));
        }
        fenceBody = this.sheepWorld.createBody(fenceBodyDef);
        PolygonShape fenceBox = new PolygonShape();
        if (type == 0) { // Horizontal
            fenceBox.setAsBox(1280 + InitConst.FENCE_THICK, InitConst.FENCE_THICK);
        } else { // Vertical
            fenceBox.setAsBox(InitConst.FENCE_THICK, 720 + InitConst.FENCE_THICK);
        }
        fenceBody.createFixture(fenceBox, 0.0f);
        fenceBox.dispose();
    }
}
