package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;

public class SheepBehavior {
    private static boolean eaten = false;
    private static long awokeTime;
    
    public static void simulate(CharacterPhysics sheep, GameScreen gameScreen) {
        processAge(sheep, gameScreen);
        if (sheep.status == 1) {
            if (gameScreen.gameWorld.gameClock.isDaytime() || eaten || sheep.starvingMode) {
                if (!simulateHunger(sheep, gameScreen, MathUtils.random(0.3f, 0.5f))) {
                    if (!moveFromWolf(sheep, gameScreen, MathUtils.random(0.3f, 0.5f))) {
                        if (!moveFromCursor(sheep, MathUtils.random(0.3f, 0.5f))) {
                            moveIdle(sheep, MathUtils.random(0.3f, 0.5f));
                        }
                    }
                }
            } else {
                driftToCenter(sheep, MathUtils.random(0.3f, 0.5f));
                sheep.bubble.state = 5;
            }
            return;
        }
        if (gameScreen.gameWorld.gameClock.isDaytime() || eaten || sheep.starvingMode) {
            if (!simulateHunger(sheep, gameScreen, MathUtils.random(0.7f, 1.1f))) {
                if (!moveFromWolf(sheep, gameScreen, MathUtils.random(0.7f, 1.1f))) {
                    if (!moveFromCursor(sheep, MathUtils.random(0.7f, 1.1f))) {
                        moveIdle(sheep, MathUtils.random(0.7f, 1.1f));
                    }
                }
            }
        } else {
            driftToCenter(sheep, MathUtils.random(0.7f, 1.1f));
            sheep.bubble.state = 5;
        }
    }
    
    private static void processAge(CharacterPhysics sheep, GameScreen gameScreen) {
        if (sheep.status == 1 && GameUtils.timerCheck(sheep.bornSeconds, InitConst.LAMB_GROW_TIME)) {
            System.out.println("Lamb Grown");
            sheep.status = 2;
            sheep.bornSeconds = TimeUtils.millis();
        } else if (sheep.status == 2 && GameUtils.timerCheck(sheep.bornSeconds, InitConst.SHEEP_WOOL_TIME)) {
            sheep.bornSeconds = TimeUtils.millis();
            gameScreen.economy.addMoney(InitConst.SHEEP_WOOL_PRICE);
        }
    }
    
    private static void driftToCenter(CharacterPhysics sheep, float multiplier) {
        GameUtils.normalizeApply(sheep, 1280 / 2.0f, 720 / 2.0f, sheep.getPosition().x, sheep.getPosition().y,
                                 InitConst.SHEEP_DRIFT_SPEED);
    }
    
    private static boolean simulateHunger(CharacterPhysics sheep, GameScreen gameScreen, float multiplier) {
        if (!GameUtils.timerCheck(awokeTime, InitConst.AWOKE_TIMEOUT)) {
            sheep.bubble.state = 6;
            return false;
        }
        float hungerDist = (InitConst.SHEEP_HUNGER_DECAY_TIME / 100.0f);
        if (GameUtils.timerCheck(sheep.eatSeconds, hungerDist * 2.0f)) {
            if (grassCheck(true, sheep, gameScreen)) {
                if (sheep.hunger >= 90) {
                    sheep.bubble.state = 0;
                } else {
                    sheep.bubble.state = 4;
                }
                sheep.hunger = sheep.hunger + InitConst.SHEEP_GRASS_FILL + MathUtils.random(-2, 3);
                if (sheep.hunger > 100) {
                    sheep.hunger = 100;
                }
                sheep.eatSeconds = TimeUtils.millis();
                return false;
            }
        }
        if (grassCheck(false, sheep, gameScreen) && sheep.hunger >= 90) {
            sheep.bubble.state = 0;
        } else if (grassCheck(false, sheep, gameScreen)) {
            sheep.bubble.state = 4;
            return false;
        }
        if (GameUtils.timerCheck(sheep.hungerSeconds, hungerDist)) {
            sheep.hunger = sheep.hunger - 1;
            sheep.hungerSeconds = TimeUtils.millis();
        }
        if (sheep.hunger > InitConst.SHEEP_LOW_FOOD) {
            sheep.starvingMode = false;
        }
        if (sheep.hunger <= InitConst.SHEEP_STARVING ||
            (sheep.starvingMode && sheep.hunger <= InitConst.SHEEP_LOW_FOOD)) {
            if (!grassCheck(false, sheep, gameScreen)) {
                sheep.bubble.state = 3;
            }
            sheep.starvingMode = true;
            moveToGrass(sheep, gameScreen, multiplier);
            return true;
        } else {
            if (sheep.bubble.state != 3 && sheep.hunger <= InitConst.SHEEP_VERY_LOW_FOOD) {
                sheep.bubble.state = 2;
            } else if (sheep.bubble.state != 3 && sheep.hunger <= InitConst.SHEEP_LOW_FOOD) {
                sheep.bubble.state = 1;
            } else if (sheep.hunger > InitConst.SHEEP_LOW_FOOD) {
                sheep.bubble.state = 0;
            }
            return false;
        }
    }
    
    private static boolean grassCheck(boolean eating, CharacterPhysics sheep, GameScreen gameScreen) {
        for (Grass grass : gameScreen.gameWorld.grassManager.grassGroup) {
            if (GameUtils.checkCircleOverlap(grass.grassPatch.getX() + (InitConst.GRASS_SIZE / 2),
                                             grass.grassPatch.getY() + (InitConst.GRASS_SIZE / 2),
                                             InitConst.GRASS_SIZE * grass.grassValue / 400 +
                                             InitConst.SHEEP_SIZE * (1.5f / (grass.grassValue) + 0.5f),
                                             sheep.getPosition().x, sheep.getPosition().y, InitConst.SHEEP_SIZE)) {
                if (eating) {
                    grass.grassValue = grass.grassValue - 0.3f;
                }
                return true;
            }
        }
        return false;
    }
    
    private static void moveToGrass(CharacterPhysics sheep, GameScreen gameScreen, float multiplier) {
        Grass targetGrass;
        boolean grassFound = false;
        float targetDistance = (float) Math.pow(Gdx.graphics.getWidth(), 2);
        if (gameScreen.gameWorld.grassManager.grassGroup.size > 0) {
            targetGrass = gameScreen.gameWorld.grassManager.grassGroup.first();
        } else {
            return;
        }
        for (Grass grass : gameScreen.gameWorld.grassManager.grassGroup) {
            if (Math.pow((sheep.getPosition().x - grass.grassPatch.getX() - InitConst.GRASS_SIZE / 2), 2) +
                Math.pow((sheep.getPosition().y - grass.grassPatch.getY() - InitConst.GRASS_SIZE / 2), 2) <
                targetDistance) {
                targetGrass = grass;
                targetDistance = (float) (
                        Math.pow((sheep.getPosition().x - grass.grassPatch.getX() - InitConst.GRASS_SIZE / 2), 2) +
                        Math.pow((sheep.getPosition().y - grass.grassPatch.getY() - InitConst.GRASS_SIZE / 2), 2));
                grassFound = true;
            }
        }
        if (grassFound) {
            GameUtils.turnsTowards(sheep, targetGrass.grassPatch.getX() + (InitConst.GRASS_SIZE / 2),
                                   targetGrass.grassPatch.getY() + (InitConst.GRASS_SIZE / 2));
            GameUtils.normalizeApply(sheep, sheep.getPosition().x, sheep.getPosition().y,
                                     targetGrass.grassPatch.getX() + (InitConst.GRASS_SIZE / 2),
                                     targetGrass.grassPatch.getY() + (InitConst.GRASS_SIZE / 2),
                                     -InitConst.SHEEP_GRASS_SPEED * multiplier);
        }
    }
    
    private static void moveIdle(CharacterPhysics sheep, float multiplier) {
        if (GameUtils.timerCheck(sheep.idleSeconds, InitConst.SHEEP_IDLE_TIME)) {
            sheep.getBody()
                 .applyAngularImpulse((MathUtils.random(-InitConst.SHEEP_IDLE_ROLL * multiplier, InitConst.SHEEP_IDLE_ROLL * multiplier)), true);
            sheep.getBody().applyForceToCenter((float) (-MathUtils.random(0.0f, (float) InitConst.SHEEP_IDLE_SPEED * multiplier) *
                                                        Math.cos(sheep.getBody().getAngle())),
                                               (float) (-MathUtils.random(0.0f, (float) InitConst.SHEEP_IDLE_SPEED * multiplier) *
                                                        Math.sin(sheep.getBody().getAngle())), true);
            sheep.idleSeconds = TimeUtils.millis() - (long) (0.5 * InitConst.SHEEP_IDLE_TIME);
        }
    }
    
    private static boolean moveFromWolf(CharacterPhysics sheep, GameScreen gameScreen, float multiplier) {
        boolean wolfFound = false;
        boolean anyWolf = false;
        for (CharacterPhysics wolf : gameScreen.gameWorld.wolfManager.wolfGroup) {
            anyWolf = true;
            if (Math.pow((wolf.getPosition().x - sheep.getPosition().x), 2) +
                Math.pow((wolf.getPosition().y - sheep.getPosition().y), 2) <
                Math.pow(InitConst.SHEEP_WOLF_DETECT, 2)) {
                GameUtils.turnsAway(sheep, wolf.getPosition().x, wolf.getPosition().y);
                GameUtils.normalizeApply(sheep, wolf.getPosition().x, wolf.getPosition().y, sheep.getPosition().x,
                                         sheep.getPosition().y, -InitConst.SHEEP_WOLF_SPEED * multiplier);
                wolfFound = true;
            }
        }
        if (!anyWolf) {
            eaten = false;
        }
        return wolfFound;
    }
    
    private static boolean moveFromCursor(CharacterPhysics sheep, float multiplier) {
        if (Math.pow((InputHandler.getX() - sheep.getPosition().x), 2) +
            Math.pow((InputHandler.getY() - sheep.getPosition().y), 2) < Math.pow(InitConst.SHEEP_CURSOR_DETECT, 2)) {
            GameUtils.turnsAway(sheep, InputHandler.getX(), InputHandler.getY());
            GameUtils.normalizeApply(sheep, InputHandler.getX(), InputHandler.getY(), sheep.getPosition().x,
                                     sheep.getPosition().y, -InitConst.SHEEP_CURSOR_SPEED * multiplier);
            return true;
        } else {
            return false;
        }
    }
    
    public static void awoke() {
        awokeTime = TimeUtils.millis();
        eaten = true;
    }
}
