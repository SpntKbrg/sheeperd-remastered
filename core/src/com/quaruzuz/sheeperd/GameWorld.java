package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

public class GameWorld {
    public World sheepWorld;
    private Box2DDebugRenderer debugRenderer;
    
    public GameScreen gameScreen;
    public GameClock gameClock;
    
    FenceManager fenceManager;
    SheepManager sheepManager;
    WolfManager wolfManager;
    GrassManager grassManager;
    
    LightingFX lighting;
    
    private long lastTime = 0;
    public boolean timeFlow = true;
    
    float physicsTimeLeft;
    
    public GameWorld(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
        Box2D.init();
        debugRenderer = new Box2DDebugRenderer();
        sheepWorld = new World(new Vector2(-0.0f, -0.0f), true);
        fenceManager = new FenceManager(sheepWorld);
        lighting = new LightingFX(sheepWorld);
        this.sheepManager = new SheepManager(this);
        this.wolfManager = new WolfManager();
        this.grassManager = new GrassManager();
        this.gameClock = new GameClock();
        
        lastTime = TimeUtils.nanoTime() / 10;
        
        // Spawn Sheep
        this.sheepManager.spawnSheep(this, InitConst.SHEEP_INIT_NUMBER);
        
        // Spawn Wolf
        this.wolfManager.spawn(this, InitConst.WOLF_INIT_NUMBER);
        
        // Grow Grass
        this.grassManager.growGrass();
        
        
    }
    
    public void render() {
        // Check GameOver
        if (checkGameOver()) {
            gameScreen.gameOver();
        }
        // Draw grass
        grassManager.render(this.gameScreen);
        // Draw characters
        sheepManager.render(this.gameScreen);
        wolfManager.render(this.gameScreen);
        // Draw cursor
        InputHandler.render(this.gameScreen);
        // The last batch draw is not rendered, hence this
        grassManager.render(this.gameScreen);
        // Box2D debug render
        debugRenderer.render(sheepWorld, gameScreen.camera.combined);
        
        // Reset last idle time
        if (lastTime + 30000000 < TimeUtils.nanoTime() / 10) {
            lastTime = TimeUtils.nanoTime() / 10;
        }
        // Next Ticks
        gameClock.runClock(timeFlow);
        boolean worldStep = fixedStep(Gdx.graphics.getDeltaTime());
        
        // Draw Box2DLight
        lighting.rayHandler.setCombinedMatrix(gameScreen.camera);
        if (worldStep) {
            lighting.rayHandler.update();
        }
        lighting.render(gameClock.gameTime);
    }
    
    private boolean checkGameOver() {
        return (sheepManager.getSheepNumber() == 0) && (gameScreen.economy.getMoney() < InitConst.LAMB_COST);
    }
    
    private boolean fixedStep(float delta) {
        physicsTimeLeft = physicsTimeLeft + delta;
        if (physicsTimeLeft > InitConst.MAX_TIME_PER_FRAME) {
            physicsTimeLeft = InitConst.MAX_TIME_PER_FRAME;
        }
        
        boolean stepped = false;
        while (physicsTimeLeft >= InitConst.TIME_STEP) {
            sheepWorld.step(InitConst.TIME_STEP, InitConst.VELOCITY_ITERS, InitConst.POSITION_ITERS);
            physicsTimeLeft = physicsTimeLeft - InitConst.TIME_STEP;
            stepped = true;
        }
        return stepped;
    }
}
