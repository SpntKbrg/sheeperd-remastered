package com.quaruzuz.sheeperd;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class SheepManager {
    
    public Array<CharacterPhysics> sheepGroup;
    private GameWorld gameWorld;
    
    public SheepManager(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        sheepGroup = new Array<CharacterPhysics>();
        
    }
    
    public void render(GameScreen gameScreen) {
        if (sheepGroup.size > 0) {
            for (CharacterPhysics sheep : sheepGroup) {
                SheepBehavior.simulate(sheep, gameScreen);
                SheepRenderer.draw(sheep, gameScreen);
                Bubble.draw(sheep, gameScreen, 1);
            }
        }
    }
    
    // Sheep Spawning
    public void spawnSheep(GameWorld gameWorld, int number) {
        for (int index = 0; index < number; index = index + 1) {
            spawnSheep(gameWorld);
        }
    }
    
    public void spawnSheep(GameWorld gameWorld) {
        spawnSheep(gameWorld, MathUtils.random(200, 1000), MathUtils.random(200, 500));
    }
    
    public void spawnSheep(GameWorld gameWorld, float posX, float posY) {
        CharacterPhysics sheep = new CharacterPhysics(gameWorld, posX, posY, InitConst.SHEEP_SIZE, InitConst.SHEEP_INIT_GRASS);
        sheep.status = 2;
        sheepGroup.add(sheep);
    }
    
    // Lamb Spawning
    public void spawnLamb(int number) {
        for (int index = 0; index < number; index = index + 1) {
            spawnLamb();
        }
    }
    
    public void spawnLamb() {
        spawnLamb(MathUtils.random(200, 1000), MathUtils.random(200, 500));
    }
    
    public void spawnLamb(float posX, float posY) {
        CharacterPhysics sheep = new CharacterPhysics(gameWorld, posX, posY, InitConst.LAMB_SIZE, InitConst.SHEEP_INIT_GRASS);
        sheep.status = 1;
        sheepGroup.add(sheep);
    }
    
    public void killSheep(CharacterPhysics sheep) {
        sheep.status = 3;
        CharacterPhysics tempSheep = sheep;
        gameWorld.sheepManager.sheepGroup.removeValue(sheep, true);
        tempSheep.getBody().setTransform(-200.0f, -200.0f, 0.0f);
        gameWorld.sheepWorld.destroyBody(tempSheep.getBody());
    }
    
    public int getSheepNumber() {
        return sheepGroup.size;
    }
}
