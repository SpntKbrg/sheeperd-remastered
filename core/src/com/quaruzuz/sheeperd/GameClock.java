package com.quaruzuz.sheeperd;

public class GameClock {
    
    float gameTime; // 0 - 86399 | 0 being Midnight
    
    public GameClock() {
        this.gameTime = InitConst.STARTING_HOUR * 3600.0f;
    }
    
    public void runClock(boolean timeFlow) {
        if (timeFlow) {
            gameTime = gameTime + InitConst.TIME_FLOW;
        }
        if (gameTime > 86399) {
            gameTime = 0;
        }
        if (isDaytime()) {
            AssetManager.gameNightMusic.stop();
            AssetManager.gameDayMusic.setLooping(true);
            AssetManager.gameDayMusic.setVolume(InitConst.MUSIC_VOL);
            AssetManager.gameDayMusic.play();
        } else {
            AssetManager.gameDayMusic.stop();
            AssetManager.gameNightMusic.setLooping(true);
            AssetManager.gameNightMusic.setVolume(InitConst.MUSIC_VOL);
            AssetManager.gameNightMusic.play();
        }
    }
    
    public boolean isDaytime() {
        return (getHour() >= 5 && getHour() < 19);
    }
    
    public boolean isNightTime() {
        return !isDaytime();
    }
    
    private float getHour() {
        return gameTime / 3600;
    }
    
    public void setHour(float hour) {
        gameTime = hour * 3600;
    }
}
