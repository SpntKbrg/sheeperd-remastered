package com.quaruzuz.sheeperd;

public class InitConst {
    
    // Mechanics Variables
    // * Speed
    public static final int SHEEP_CURSOR_SPEED = 150000;
    public static final int SHEEP_WOLF_SPEED = 200000;
    public static final int SHEEP_GRASS_SPEED = 220000;
    public static final int SHEEP_IDLE_SPEED = 80000;
    public static final int SHEEP_DRIFT_SPEED = 500;
    
    public static final float SHEEP_IDLE_ROLL = 7000;
    
    public static final int WOLF_CURSOR_SPEED = 200000;
    public static final int WOLF_SHEEP_SPEED = 300000;
    public static final int WOLF_HUNGRY_MOD = 100000;
    public static final int WOLF_IDLE_SPEED = 70000;
    
    public static final float WOLF_IDLE_ROLL = 14000;
    
    // * Timing
    public static final float SHEEP_IDLE_TIME = 0.5f;
    public static final float WOLF_IDLE_TIME = 0.3f;
    public static final float GRASS_GROW_TIME = 50.0f;
    
    // * Detection Range
    public static final int SHEEP_WOLF_DETECT = 50;
    public static final int SHEEP_CURSOR_DETECT = 140;
    public static final int WOLF_SHEEP_DETECT = 800;
    public static final int WOLF_CURSOR_DETECT = 80;
    
    // Difficulty Variables
    public static final float OVERLAP_ALLOW = 1.5f;
    public static final int WOLF_SPAWNS = 5;
    // * Time Stuff
    public static final int GRASS_SPAWN_TIME = 240;
    public static final float LAMB_GROW_TIME = 240;
    public static final float SHEEP_WOOL_TIME = 60;
    public static final float TIME_FLOW = 20; // Game Speed
    // * Game Lights
    public static final float TORCH_SIZE = 200.0f;
    public static final float EYE_GLOW = 0.0f;
    public static final float WOLF_EYE_GLOW = 500.0f;
    public static final float EYE_CONE = 10.0f;
    // * Game Costs
    public static final long LAMB_COST = 150;
    public static final long SHEEP_WOOL_PRICE = 2;
    public static final long GRASS_COST = 40;
    // * Sheep Hunger
    public static final int SHEEP_INIT_GRASS = 50;
    public static final int SHEEP_STARVING = 10;
    public static final int SHEEP_VERY_LOW_FOOD = 20;
    public static final int SHEEP_LOW_FOOD = 40;
    public static final int SHEEP_HUNGER_DECAY_TIME = 50; // Seconds took to reduce full hunger to 0
    public static final int SHEEP_GRASS_FILL = 5;
    // * Wolf Hunger
    public static final int WOLF_HUNGER_DECAY_TIME = 30;
    public static final int WOLF_INIT_HUNGER = 100;
    
    // Interface Variables
    public static final int FENCE_THICK = 15;
    public static final int TOOLBOX_SIZE = 125;
    public static final float BUTTON_WIDTH = 80;
    public static final float BUTTON_HEIGHT = 60;
    public static final float AWOKE_TIMEOUT = 5f;
    
    // Volume
    public static final float MUSIC_VOL = 1.0f;
    
    // Calculation Variables
    public static final int SHEEP_SIZE = 12;
    public static final int LAMB_SIZE = 10;
    public static final int WOLF_SIZE = 14;
    public static final int GRASS_SIZE = 256;
    
    // Performance Variables
    // * Graphics
    public static final int SUNRAYS = 256;
    public static final int EYERAYS = 4;
    public static final int BLUR_AMOUNT = 1;
    // * Game Performance
    private final static int MAX_FPS = 60;
    private final static int MIN_FPS = 20;
    public final static float TIME_STEP = 1f / MAX_FPS;
    public final static float MAX_STEPS = 1f + MAX_FPS / MIN_FPS;
    public final static float MAX_TIME_PER_FRAME = TIME_STEP * MAX_STEPS;
    public final static int VELOCITY_ITERS = 6;
    public final static int POSITION_ITERS = 2;
    
    // Game Starting Variables
    public static final int SHEEP_INIT_NUMBER = 10;
    public static final int WOLF_INIT_NUMBER = 0;
    public static final float STARTING_HOUR = 5;
}
