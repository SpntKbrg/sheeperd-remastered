package com.quaruzuz.sheeperd;

public class WolfRenderer {
    public static void draw(CharacterPhysics wolf, GameScreen gameScreen) {
        gameScreen.batch.draw(AssetManager.wolfTexture, wolf.getBody().getPosition().x - 25, wolf.getBody()
                                                                                                 .getPosition().y - 10,
                              (float) 25, 10.5f, 50, 21, (float) (0.08 * InitConst.WOLF_SIZE),
                              (float) (0.08 * InitConst.WOLF_SIZE), (float) Math.toDegrees(wolf.getBody().getAngle()), 0, 0,
                              50, 21, false, false);
    }
}
