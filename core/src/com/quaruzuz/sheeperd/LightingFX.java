package com.quaruzuz.sheeperd;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;

public class LightingFX {
    
    public RayHandler rayHandler;
    
    public PointLight torch;
    
    private float temperature = 0;
    
    public LightingFX(World sheepWorld) {
        RayHandler.setGammaCorrection(true);
        RayHandler.useDiffuseLight(true);
        
        rayHandler = new RayHandler(sheepWorld);
        rayHandler.setAmbientLight(0.30f, 0.30f, 0.30f, 0.5f);
        rayHandler.setBlurNum(InitConst.BLUR_AMOUNT);
        rayHandler.setShadows(true);
        
        torch = new PointLight(rayHandler, InitConst.SUNRAYS, new Color(1f, 1f, 1f, 0.8f), InitConst.TORCH_SIZE, 0, 0);
    }
    
    public void render(float time) {
        torchControl();
        rayHandler.setAmbientLight(calculateColor(time));
        rayHandler.updateAndRender();
    }
    
    private void torchControl() {
        torch.setPosition(InputHandler.getX(), InputHandler.getY());
    }
    
    private float checkColor(float colorIn) {
        if (colorIn < 0) {
            colorIn = 0.0f;
        } else if (colorIn > 255.0) {
            colorIn = 255.0f;
        }
        return colorIn;
    }
    
    private Color tempToColor() {
        float red;
        float green;
        float blue;
        temperature = temperature / 100;
        // The magic happens here :P source : http://www.tannerhelland.com/4435/convert-temperature-rgb-algorithm-code/
        if (temperature <= 66) {
            red = 255.0f;
            green = checkColor((float) (99.4708025861 * Math.log(temperature) - 161.1195681661));
            if (temperature < 19) {
                blue = 0.0f;
            } else {
                blue = checkColor((float) (138.5177312231 * Math.log(temperature - 10) - 305.0447927307));
            }
        } else {
            red = checkColor(329.698727446f * (float) (Math.pow((temperature - 60), -0.1332047592)));
            green = checkColor((float) (288.1221695283 * Math.pow((temperature - 60), -0.0755148492)));
            blue = 255.0f;
        }
    
        return new Color(red / 255.0f, green / 255.0f, blue / 255.0f, 1.0f);
    }
    
    private Color calculateColor(float time) {
        float hour = time / 3600.0f;
        torch.setActive(false);
        if (hour >= 5.0f && hour <= 5.1f) {
            temperature = 5600;
        } else {
            temperature = temperature * 100;
        }
        if (hour > 5.0f && hour < 18.0f) { // Daytime
            temperature = temperature - 0.01f;
            return tempToColor();
        } else if ((hour >= 18.0f && hour < 19.0f) || (hour >= 4.0f && hour < 5.0f)) { // Dusk & Dawn Transition
            temperature = temperature + 100;
            Color transition = tempToColor();
            if (hour >= 18.0) {
                hour = Math.abs(hour - 18.5f) * 2;
            } else {
                hour = Math.abs(hour - 4.5f) * 2;
            }
            transition.set(transition.r * hour, transition.g * hour, transition.b * hour, transition.a);
            return transition;
        } else { // Nighttime
            torch.setActive(true);
            temperature = temperature - 1;
            Color transition = tempToColor();
            transition.set(transition.r * 0.35f, transition.g * 0.35f, transition.b * 0.42f, transition.a);
            return transition;
        }
    }
}
