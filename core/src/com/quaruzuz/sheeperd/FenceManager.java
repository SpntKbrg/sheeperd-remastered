package com.quaruzuz.sheeperd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class FenceManager {
    private Array<Fence> fenceList;
    private World sheepWorld;
    
    public FenceManager(World sheepWorld) {
        this.sheepWorld = sheepWorld;
        fenceList = new Array<Fence>();
        // Add Horizontal Fences (Bottom, Top)
        fenceList.add(new Fence(this.sheepWorld, 0, 0.0f));
        fenceList.add(new Fence(this.sheepWorld, 0, 720 - InitConst.FENCE_THICK));
        // Add Vertical Fences (Left, Right)
        fenceList.add(new Fence(this.sheepWorld, 1, 0.0f));
        fenceList.add(new Fence(this.sheepWorld, 1,
                                1280 - InitConst.TOOLBOX_SIZE - InitConst.FENCE_THICK));
    }
}
