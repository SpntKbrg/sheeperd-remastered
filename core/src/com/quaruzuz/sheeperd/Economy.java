package com.quaruzuz.sheeperd;

public class Economy {
    private long money = 0;
    public Economy() {
        money = 500;
    }
    public long getMoney() {
        return money;
    }
    public void spendMoney(long cost) {
        money = money - cost;
    }
    public void addMoney(long cost) {
        money = money + cost;
    }
}
